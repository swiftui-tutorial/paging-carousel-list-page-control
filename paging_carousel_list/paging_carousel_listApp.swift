//
//  paging_carousel_listApp.swift
//  paging_carousel_list
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

@main
struct paging_carousel_listApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
